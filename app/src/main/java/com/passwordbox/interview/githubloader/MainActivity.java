package com.passwordbox.interview.githubloader;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.os.Build;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.TextView;


public class MainActivity extends Activity implements View.OnClickListener {

    ProgressDialog dialog;
    private View resultSection;
    private EditText edtSearch;

    private TextView txtLogin;
    private TextView txtUsername;
    private TextView txtEmail;
    private TextView txtLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn_search).setOnClickListener(this);
        resultSection = findViewById(R.id.lyt_result);
        edtSearch = (EditText) findViewById(R.id.edt_search);

        txtLogin = (TextView) findViewById(R.id.txt_login);
        txtUsername = (TextView) findViewById(R.id.txt_username);
        txtEmail = (TextView) findViewById(R.id.txt_email);
        txtLocation = (TextView) findViewById(R.id.txt_location);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch(id) {
            case R.id.btn_search:
                displayDialog(view);
            break;
        }
    }

    private void displayDialog(View view) {
        dialog = new CustomProgressDialog(view.getContext());
        dialog.show();
    }

    private void updateResults() {
        txtLogin.setText(R.string.unknown);
        txtUsername.setText(R.string.unknown);
        txtEmail.setText(R.string.unknown);
        txtLocation.setText(R.string.unknown);
    }

    private class CustomProgressDialog extends ProgressDialog {

        public CustomProgressDialog(Context context) {
            super(context);
            setMessage(getString(R.string.loading));
        }

        public CustomProgressDialog(Context context, int theme) {
            super(context, theme);
            setMessage(getString(R.string.loading));
        }

        @Override
        public void onBackPressed() {
            super.onBackPressed();
            resultSection.setVisibility(View.VISIBLE);
            updateResults();
        }
    }
}
